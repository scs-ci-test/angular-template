FROM nginx:1.21.4-alpine
COPY dist/angular-template /usr/share/nginx/html
EXPOSE 80
